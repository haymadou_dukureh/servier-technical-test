#Libraries
from jsoncomment import JsonComment
import json
import pandas as pd
import yaml
#Preprocessing functions

def read_csv(filepath):
    return pd.read_csv(filepath)

def read_ymlFile(filepath):
    with open(filepath,'r') as file:
        filesPath = yaml.safe_load(file)
    return filesPath

def json_to_df(filepath):
    with open(filepath) as data_file:
        parser = JsonComment(json)  # handle trailing comma in json
        data = parser.load(data_file)
    return pd.DataFrame(data)


def delete_chars(df): #To remove special characters like ( - ..
    df = df.map(lambda s: s.replace("\\xc3\\xb1", "") if type(s) == str else s) 
    df = df.map(lambda s: s.replace("\\xc3\\x28", "") if type(s) == str else s)
    return df


def lower_df(df):
    return df.map(lambda s: s.lower() if type(s) == str else s)


def format_date(df, column): #To normalize every different date format from df
    df[column] = pd.to_datetime(df[column],format='mixed')
    return df


def join_drugs_and_title(drugs_df, title_df):
    r = '({})'.format('|'.join(drugs_df.drug))
    merge_df = title_df.title.str.extract(r, expand=False).fillna(title_df.title)
    title_mention = drugs_df.merge(title_df, left_on='drug', right_on=merge_df, how='inner')
    return title_mention