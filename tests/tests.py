import pytest
import sys
sys.path.append('../Servier-technical-test/utils') 
 
import pandas as pd
from utils import delete_chars,lower_df,format_date,join_drugs_and_title

string_data = "name, date_birth, city\nJohn, 15-11-2001, New(York\nAlice, 13-01-1978, Los Angeles\nBob, 12-10-1985, Chicago"
list_data = [row.split(", ") for row in string_data.split("\n")]
string_drugs = "atccode,drug\nA04AD,DIPHENHYDRAMINE\nS03AA,TETRACYCLINE\nV03AB,ETHANOL\nA03BA,ATROPINE\nA01AD,EPINEPHRINE\n6302001,ISOPRENALINE\nR01AD,BETAMETHASONE"
string_articles ='title,journal\nUse of Diphenhydramine as an Adjunctive Sedative for Colonoscopy in Patients Chronically on Opioids","Journal of emergency nursing"'
global df
global df_drugs
global df_articles
df = pd.DataFrame(list_data[1:], columns=list_data[0])
df_drugs = pd.DataFrame(string_drugs[1],columns = string_drugs[0])
df_articles = pd.DataFrame(string_articles[1],columns = string_articles[0])

    
def testDeleteChars():
    a = delete_chars(df)
    assert a.city[0] == 'NewYork'
   

def testLowerDf():
    b = lower_df(df)
    assert b.name[2] == 'bob'

def testFormatDate():
    c = format_date(df,"date")
    assert c.date[1] == '13/01/1978'

def testJoin_Drugs_And_Title():

    df_drugs_test = lower_df(df_drugs)
    df_articles_test = lower_df(df_articles)

    df_drugs_test = delete_chars(df_drugs)
    df_articles_test = delete_chars(df_articles)

    df_articles_test = format_date(df_articles)
    df_drugs_test = format_date(df_drugs)

    d = join_drugs_and_title(df_articles_test,df_articles_test)
    assert d.drug[0] == 'diphenhydramine'

