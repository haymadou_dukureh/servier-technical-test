**Rendu test technique**

**Installation:**


virtualenv
python 3.11.5

    virtualenv venv
    . venv/bin/activate
    pip install -r requirements.txt
    python main.py

Le programme utilise les fichiers csv et json dans le répertoire "ressources" et va les transformer conformément aux règles de gestions de manière exploitable afin d'obtenir en sortie un fichier Json. Il extrait également le nom du journal qui mentionne le plus de médicaments différents depuis le json produit.

Le json est une liste de dict. Chaque dict représente un médicament. Chaque médicament a une liste de mentions (date, type d'article, titrte de la publicaction (sauf si c'est un journal0 et un jourbal)) où il a été répertorié. 

**Partie SQL**

SELECT
    date,
    SUM(prod_price * prod_qty) AS ventes
FROM
    transaction
WHERE
    date BETWEEN "2019-01-01"AND "2019-12-31"
GROUP BY
    date
ORDER BY
    date ASC

-------------------------------------------------

SELECT
  client_id,
  SUM(CASE
    WHEN product.product_type = "MEUBLE" THEN prod_price * prod_qty END)
    AS ventes_meuble,
  SUM(CASE
    WHEN product.product_type = "DECO" THEN prod_price * prod_qty END)
    AS ventes_deco,
FROM
    transaction
JOIN
    product_nomenclature AS product
ON
    transaction.prod_id=product.product_id
WHERE
    date BETWEEN "2020-01-01" AND "2020-12-31"
GROUP BY
    client_id

----------------------------------------------------

_Question 4:_
Extraire depuis le json produit par la data pipeline le nom du journal qui mentionne le plus de médicaments différents ?

"journal of emergency nursing" avec 2 médicaments différents

_Question 6:_
Quels sont les éléments à considérer pour faire évoluer votre code afin qu’il puisse gérer de grosses volumétries de données (fichiers de plusieurs To ou millions de fichiers par exemple) ?

Dans un premier temps, je réfléchis à la finalité de mon code et à mes ressources.
Cloud ou on-premise ?
Pour qui ? Est-ce que mon application est plutôt scalable ?
Quelles contraintes de temps ? => Améliorer l'optimisaiton de la pipeline pour être en phase.
Cost efficient ou efficace ? => Quel budget ?


Pourriez-vous décrire les modifications qu’il faudrait apporter, s’il y en a, pour prendre en considération de telles volumétries ?

- Utiliser des services serverless
- Si on a la possibilité, on peut s'orienter dans un premier temps vers des fichiers plus adaptés à des gros volumes de données (Avro par exemple) car des csv peuvent vite devenir contreproductifs pour de grosses volumétries.
- Utiliser des framework de traitement en parallèle comme Spark pour améliorer les performances.
- Conteneuriser l'application avec Docker pour la lancer via une CI/CD, la rendant plus disponible et continue. Et utiliser Kubernetes afin d'automatiser la scalabilité de l'application

